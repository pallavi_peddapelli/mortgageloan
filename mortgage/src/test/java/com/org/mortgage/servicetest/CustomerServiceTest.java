package com.org.mortgage.servicetest;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.mortgage.dto.CustomerDto;
import com.org.mortgage.dto.CustomerResponseDto;
import com.org.mortgage.entity.Account;
import com.org.mortgage.entity.Customer;
import com.org.mortgage.entity.Loan;
import com.org.mortgage.exception.CustomerNotPresent;
import com.org.mortgage.exception.PasswordIncorrect;
import com.org.mortgage.repository.AccountRepository;
import com.org.mortgage.repository.CustomerRepository;
import com.org.mortgage.repository.LoanRepository;
import com.org.mortgage.service.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

	@Mock
	AccountRepository accountRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	LoanRepository loanRepository;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	CustomerDto customerDto;
	CustomerDto customerDtoIn;
	CustomerDto customerDtoInn;
	CustomerResponseDto customerResponseDto;
	Customer customer;
	Customer customer2;
	Account account;
	Loan loan;

	@Before
	public void setUp() {
		customer2 = new Customer();
		
		customer2.setCustomerId(1000);
		customer2.setPassword("123");
		customer2.setUserId(10000000);
		
		customerDto = new CustomerDto();
		customerDto.setCustomerId(1);
		customerDto.setPassword("123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setMortgageAmount(-300000);
		customerResponseDto.setSavingsAccount(48749934);
		customerResponseDto.setSavingsAmount(200000);
		customerResponseDto.setUserId(1);
		customerResponseDto.setStatus(625);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setPassword("123");
		customer.setUserId(1);

		account = new Account();
		account.setAccountId(1);
		account.setSavingsAccount(48749934);
		account.setSavingsAmount(200000);
		account.setUserId(1);

		loan = new Loan();
		loan.setMortgageAmount(-300000);

		customerDtoIn = new CustomerDto();
		customerDtoIn.setCustomerId(1);
		customerDtoIn.setPassword("1234");

		customerDtoInn = new CustomerDto();
		customerDtoInn.setCustomerId(10);
		customerDtoInn.setPassword("123");
	}

	@Test
	public void customerLoginTest() throws CustomerNotPresent, PasswordIncorrect {

		Mockito.when(customerRepository.findByCustomerId(Mockito.anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByUserId(Mockito.anyLong())).thenReturn(Optional.of(account));
		Mockito.when(loanRepository.findByUserId(Mockito.anyLong())).thenReturn(Optional.of(loan));
		CustomerResponseDto response = customerServiceImpl.customerLogin(customerDto);
		Assert.assertEquals(customerResponseDto.getStatus(), response.getStatus());
	}

	@Test(expected = PasswordIncorrect.class)
	public void customerLoginPasswordErrorTest() throws CustomerNotPresent, PasswordIncorrect {

		Mockito.when(customerRepository.findByCustomerId(Mockito.anyLong())).thenReturn(Optional.of(customer));
		customerServiceImpl.customerLogin(customerDtoIn);

	}

	@Test(expected = CustomerNotPresent.class)
	public void customerLoginTestCustomerNotPresentTest() throws CustomerNotPresent, PasswordIncorrect {

		Mockito.lenient().when(customerRepository.findByCustomerId(2l)).thenReturn(Optional.of(customer2));
		customerServiceImpl.customerLogin(customerDtoInn);

	}

}
