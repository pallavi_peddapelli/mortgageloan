package com.org.mortgage.servicetest;

import java.util.Date;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.mortgage.dto.PropertyResponseDto;
import com.org.mortgage.dto.OccupationDetailsRequestDto;
import com.org.mortgage.dto.OccupationDetailsResponseDto;
import com.org.mortgage.dto.PersonalDetailsRequestDto;
import com.org.mortgage.dto.PersonalDetailsResponseDto;
import com.org.mortgage.dto.PropertyDto;
import com.org.mortgage.entity.Customer;
import com.org.mortgage.entity.OccupationDetails;
import com.org.mortgage.entity.Property;
import com.org.mortgage.entity.SquareFeetDetails;
import com.org.mortgage.entity.User;
import com.org.mortgage.exception.DateOfBirthException;
import com.org.mortgage.exception.PropertyException;
import com.org.mortgage.exception.PropertyValueException;
import com.org.mortgage.exception.SalaryException;
import com.org.mortgage.exception.SquareFettDetailsException;
import com.org.mortgage.exception.UserException;
import com.org.mortgage.exception.UserNotEligibleException;
import com.org.mortgage.repository.CustomerRepository;
import com.org.mortgage.repository.OccupationDetailsRepository;
import com.org.mortgage.repository.PropertyRepositry;
import com.org.mortgage.repository.SquareFeetDetailsRepositry;
import com.org.mortgage.repository.UserRepository;
import com.org.mortgage.service.UserServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Mock
	UserRepository userRepository;

	@Mock
	OccupationDetailsRepository occupationDetailsRepository;

	@Mock
	PropertyRepositry propertyRepositry;

	@Mock
	SquareFeetDetailsRepositry squareFeetDetailsRepositry;

	@Mock
	CustomerRepository customerRepositry;

	@InjectMocks
	UserServiceImpl userServiceImpl;

	User user;
	User user1;
	PersonalDetailsResponseDto personalDetailsResponseDto;
	PersonalDetailsRequestDto personalDetailsRequestDto;
	OccupationDetails occupationalDetails;
	OccupationDetailsRequestDto occupationDetailsRequestDto;
	OccupationDetailsResponseDto occupationDetailsResponseDto;
	OccupationDetails occupationalDetails2;
	SquareFeetDetails squareFeetDetails;
	User userU;
	OccupationDetails occupationDetails;
	Property property;
	Customer customer;
	PropertyDto propertyDto;
	PropertyResponseDto loginResponseDto;
	Date date;

	@SuppressWarnings("deprecation")
	@Before
	public void setup() {
		user = new User();
		user.setDateOfBirth(new Date());
		user.setGender("male");
		user.setPanCard("CULPK0909");
		user.setPhoneNumber("92837465");
		user.setUserId(1l);
		user.setUserName("Tom");

		user1 = new User();
		personalDetailsRequestDto = new PersonalDetailsRequestDto();
		personalDetailsResponseDto = new PersonalDetailsResponseDto();
		occupationalDetails = new OccupationDetails();
		occupationalDetails.setDesgination("SSE");
		occupationalDetails.setOccupationDetailsId(1l);
		occupationalDetails.setOrganizationAddress("Bangalore");
		occupationalDetails.setOrganizationName("ABC");
		occupationalDetails.setSalary(500000);
		occupationalDetails.setUserId(1l);

		occupationalDetails2 = new OccupationDetails();
		occupationalDetails2.setDesgination("SSE");
		occupationalDetails2.setOccupationDetailsId(1l);
		occupationalDetails2.setOrganizationAddress("Bangalore");
		occupationalDetails2.setOrganizationName("ABC");
		occupationalDetails2.setSalary(500000);
		occupationalDetails2.setUserId(2l);
		occupationDetailsRequestDto = new OccupationDetailsRequestDto();
		date = new Date();
		date.setYear(24);
		squareFeetDetails = new SquareFeetDetails();
		squareFeetDetails.setPerSquareFeetValue(1000);
		squareFeetDetails.setPinCode(501445);
		squareFeetDetails.setSquareFeetDetailsId(1);

		userU = new User();
		userU.setUserName("venu");

		userU.setDateOfBirth(date);
		userU.setGender("male");
		userU.setPanCard("CULPK4567");
		userU.setPhoneNumber("9876543");
		userU.setUserId(1);

		occupationDetails = new OccupationDetails();
		occupationDetails.setDesgination("software");
		occupationDetails.setOccupationDetailsId(1);
		occupationDetails.setOrganizationAddress("bangalore");
		occupationDetails.setOrganizationName("org");
		occupationDetails.setSalary(10000);
		occupationDetails.setUserId(1);

		property = new Property();
		property.setPropertyId(1);
		property.setPropertyName("home");
		property.setPropertyValue(10000);
		property.setSquareFeetId(1);
		property.setUserId(1);

		customer = new Customer();
		customer.setPassword("23456");
		customer.setUserId(1);

		propertyDto = new PropertyDto();
		propertyDto.setArea(300);
		//propertyDto.setDepositValue(1000);
		propertyDto.setLoan(10000);
		propertyDto.setTenureMonths(12);
		propertyDto.setPropertyName("home");
		propertyDto.setUserId(1);

		loginResponseDto = new PropertyResponseDto();
		//loginResponseDto.setLoginId(1);
		//loginResponseDto.setPassword("23456");
		//loginResponseDto.setMessage("Your loggin successfully done");
		loginResponseDto.setStatus(701);

		date = new Date();
		date.setYear(24);

		squareFeetDetails = new SquareFeetDetails();
		squareFeetDetails.setPerSquareFeetValue(10);
		squareFeetDetails.setPinCode(501445);
		squareFeetDetails.setSquareFeetDetailsId(1);

		user = new User();
		user.setUserName("venu");

		user.setDateOfBirth(date);
		user.setGender("male");
		user.setPanCard("CULPK4567");
		user.setPhoneNumber("9876543");
		user.setUserId(1);

		occupationDetails = new OccupationDetails();
		occupationDetails.setDesgination("software");
		occupationDetails.setOccupationDetailsId(1);
		occupationDetails.setOrganizationAddress("bangalore");
		occupationDetails.setOrganizationName("org");
		occupationDetails.setSalary(10000);
		occupationDetails.setUserId(1);

		property = new Property();
		property.setPropertyId(1);
		property.setPropertyName("home");
		property.setPropertyValue(10000);
		property.setSquareFeetId(1);
		property.setUserId(1);

		customer = new Customer();
		customer.setPassword("23456");
		customer.setUserId(1);

		propertyDto = new PropertyDto();
		propertyDto.setArea(300);
		//propertyDto.setDepositValue(1000);
		propertyDto.setLoan(10000);
		propertyDto.setTenureMonths(12);
		propertyDto.setPropertyName("home");
		propertyDto.setUserId(1);
		propertyDto.setPincode(501445);

		loginResponseDto = new PropertyResponseDto();
		//loginResponseDto.setLoginId(1);
		//loginResponseDto.setPassword("23456");
		//loginResponseDto.setMessage("Your loggin successfully done");
		loginResponseDto.setStatus(701);
	}

	/*
	 * @Test public void userPersonalDetailsTest() throws DateOfBirthException {
	 * Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
	 * PersonalDetailsResponseDto actualValue =
	 * userServiceImpl.userPersonalDetails(personalDetailsRequestDto);
	 * Assert.assertNotNull(actualValue); Assert.assertEquals(1l,
	 * actualValue.getUserId()); }
	 */

	@Test
	public void userOccupationDetailsTest() throws UserException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.lenient().when(occupationDetailsRepository.findByUserId(1l))
				.thenReturn(Optional.of(occupationalDetails2));

		Mockito.when(occupationDetailsRepository.save(Mockito.any(OccupationDetails.class)))
				.thenReturn((occupationalDetails2));

		OccupationDetailsResponseDto actualValue = userServiceImpl.userOccupationDetails(occupationDetailsRequestDto);
		Assert.assertNotNull(actualValue);
		Assert.assertEquals(2l, actualValue.getUserId());
	}

	@Test(expected = UserException.class)
	public void userOccupationDetailsUserAlreadyExistsTest() throws UserException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(occupationDetailsRepository.findByUserId(Mockito.anyLong()))
				.thenReturn(Optional.of(occupationalDetails2));

		Mockito.lenient().when(occupationDetailsRepository.save(Mockito.any(OccupationDetails.class)))
				.thenReturn((occupationalDetails));

		userServiceImpl.userOccupationDetails(occupationDetailsRequestDto);
	}

	@Test(expected = UserException.class)
	public void userOccupationDetailsUserExistsTest() throws UserException {
		Mockito.lenient().when(userRepository.findById(1l)).thenReturn(Optional.of(user1));
		userServiceImpl.userOccupationDetails(occupationDetailsRequestDto);
	}

	

	@Test(expected = SquareFettDetailsException.class)
	public void squareFettDetailsException() throws SquareFettDetailsException, UserNotEligibleException,
			PropertyValueException, DateOfBirthException, SalaryException, PropertyException {
		Mockito.when(squareFeetDetailsRepositry.findByPinCode(Mockito.anyInt())).thenReturn(null);

		userServiceImpl.propertyDetailsIn(propertyDto);

	}

	@Test(expected = PropertyValueException.class)
	public void propertyValueException() throws SquareFettDetailsException, UserNotEligibleException,
			PropertyValueException, DateOfBirthException, SalaryException, PropertyException {
		Mockito.when(squareFeetDetailsRepositry.findByPinCode(Mockito.anyInt())).thenReturn(squareFeetDetails);

		userServiceImpl.propertyDetailsIn(propertyDto);

	}

}
