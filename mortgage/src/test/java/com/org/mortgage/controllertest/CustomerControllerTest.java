package com.org.mortgage.controllertest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.mortgage.controller.LoginController;
import com.org.mortgage.dto.CustomerDto;
import com.org.mortgage.dto.CustomerResponseDto;
import com.org.mortgage.exception.CustomerNotPresent;
import com.org.mortgage.exception.PasswordIncorrect;
import com.org.mortgage.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {
	@Mock
	CustomerService customerService;

	@InjectMocks
	LoginController loginController;

	CustomerDto customerDto;
	CustomerResponseDto customerResponseDto;

	@Before
	public void setUp() {
		customerDto = new CustomerDto();
		customerDto.setCustomerId(1);
		customerDto.setPassword("123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setMortgageAmount(-300000);
		customerResponseDto.setSavingsAccount(48749934);
		customerResponseDto.setSavingsAmount(200000);
		customerResponseDto.setUserId(1);
		customerResponseDto.setStatus(625);
	}

	@Test
	public void customerLogin() throws CustomerNotPresent, PasswordIncorrect {

		Mockito.when(customerService.customerLogin(Mockito.any(CustomerDto.class))).thenReturn(customerResponseDto);
		ResponseEntity<CustomerResponseDto> response = loginController.customerLogin(customerDto);
		Assert.assertEquals(customerResponseDto, response.getBody());
	}
}
