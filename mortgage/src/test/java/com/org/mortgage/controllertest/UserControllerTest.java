package com.org.mortgage.controllertest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.mortgage.controller.UserController;
import com.org.mortgage.dto.OccupationDetailsRequestDto;
import com.org.mortgage.dto.OccupationDetailsResponseDto;
import com.org.mortgage.dto.PersonalDetailsRequestDto;
import com.org.mortgage.dto.PersonalDetailsResponseDto;
import com.org.mortgage.exception.DateOfBirthException;
import com.org.mortgage.exception.UserException;
import com.org.mortgage.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	@Mock
	UserService userService;

	@InjectMocks
	UserController userController;

	PersonalDetailsResponseDto personalDetailsResponseDto;
	PersonalDetailsRequestDto personalDetailsRequestDto;
	OccupationDetailsRequestDto occupationDetailsRequestDto;
	OccupationDetailsResponseDto occupationDetailsResponseDto;

	@Before
	public void setup() {
		personalDetailsResponseDto = new PersonalDetailsResponseDto();
		personalDetailsRequestDto = new PersonalDetailsRequestDto();
		occupationDetailsRequestDto = new OccupationDetailsRequestDto();
		occupationDetailsResponseDto = new OccupationDetailsResponseDto();
	}

	@Test
	public void userPersonalDetailsTest() throws DateOfBirthException {
		Mockito.when(userService.userPersonalDetails(Mockito.any(PersonalDetailsRequestDto.class)))
				.thenReturn(personalDetailsResponseDto);
		ResponseEntity<PersonalDetailsResponseDto> actualValue = userController
				.userPersonalDetails(personalDetailsRequestDto);
		Assert.assertEquals(personalDetailsResponseDto, actualValue.getBody());
	}

	@Test
	public void userOccupationDetailsTest() throws UserException {
		Mockito.when(userService.userOccupationDetails(Mockito.any(OccupationDetailsRequestDto.class)))
				.thenReturn(occupationDetailsResponseDto);
		ResponseEntity<OccupationDetailsResponseDto> actualValue = userController
				.userOccupationDetails(occupationDetailsRequestDto);

		Assert.assertEquals(occupationDetailsResponseDto, actualValue.getBody());
	}

}
