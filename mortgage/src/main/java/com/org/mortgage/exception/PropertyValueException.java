package com.org.mortgage.exception;

public class PropertyValueException extends Exception {
	private static final long serialVersionUID = 1L;

	public PropertyValueException(String message) {
		super(message);
	}

}
