package com.org.mortgage.exception;

public class SalaryException extends Exception {
	private static final long serialVersionUID = 1L;

	public SalaryException(String message) {
		super(message);
	}
}
