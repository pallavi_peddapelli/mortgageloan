package com.org.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgage.entity.SquareFeetDetails;

@Repository
public interface SquareFeetDetailsRepositry extends JpaRepository<SquareFeetDetails, Long> {
	SquareFeetDetails findByPinCode(int pincode);
}
