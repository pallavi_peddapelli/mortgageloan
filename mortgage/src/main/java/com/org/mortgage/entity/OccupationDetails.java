package com.org.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class OccupationDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long occupationDetailsId;
	private long userId;
	private double salary;
	private String desgination;
	private String organizationName;
	private String organizationAddress;
	public long getOccupationDetailsId() {
		return occupationDetailsId;
	}
	public void setOccupationDetailsId(long occupationDetailsId) {
		this.occupationDetailsId = occupationDetailsId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getDesgination() {
		return desgination;
	}
	public void setDesgination(String desgination) {
		this.desgination = desgination;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getOrganizationAddress() {
		return organizationAddress;
	}
	public void setOrganizationAddress(String organizationAddress) {
		this.organizationAddress = organizationAddress;
	}

}
