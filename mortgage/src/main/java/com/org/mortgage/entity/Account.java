package com.org.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long accountId;
	private long savingsAccount;
	private long mortgageAccount;
	private double mortgageAmount;
	public long getMortgageAccount() {
		return mortgageAccount;
	}
	public void setMortgageAccount(long mortgageAccount) {
		this.mortgageAccount = mortgageAccount;
	}
	public double getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	private double savingsAmount;
	private long userId;
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public long getSavingsAccount() {
		return savingsAccount;
	}
	public void setSavingsAccount(long savingsAccount) {
		this.savingsAccount = savingsAccount;
	}
	public double getSavingsAmount() {
		return savingsAmount;
	}
	public void setSavingsAmount(double savingsAmount) {
		this.savingsAmount = savingsAmount;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

}
