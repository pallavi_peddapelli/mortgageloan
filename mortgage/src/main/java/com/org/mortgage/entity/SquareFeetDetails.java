package com.org.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class SquareFeetDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long squareFeetDetailsId;
	private double perSquareFeetValue;
	private int pinCode;
	public long getSquareFeetDetailsId() {
		return squareFeetDetailsId;
	}
	public void setSquareFeetDetailsId(long squareFeetDetailsId) {
		this.squareFeetDetailsId = squareFeetDetailsId;
	}
	public double getPerSquareFeetValue() {
		return perSquareFeetValue;
	}
	public void setPerSquareFeetValue(double perSquareFeetValue) {
		this.perSquareFeetValue = perSquareFeetValue;
	}
	public int getPinCode() {
		return pinCode;
	}
	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

}
