
package com.org.mortgage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgage.dto.LoanRequestDto;
import com.org.mortgage.dto.LoanResponseDto;
import com.org.mortgage.dto.TransactionResponseDto;
import com.org.mortgage.exception.SavingAccountException;
import com.org.mortgage.exception.SavingAmountException;
import com.org.mortgage.service.TransactionService;

@RequestMapping("/transations")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class Trasactioncontroller {

	@Autowired
	TransactionService transactionService;

	@GetMapping("/{customerId}")
	public ResponseEntity<TransactionResponseDto> transactionList(@PathVariable long customerId) {

		return new ResponseEntity<>(transactionService.transactionList(customerId), HttpStatus.ACCEPTED);
	}

	@PostMapping
	public ResponseEntity<LoanResponseDto> loanPayment(@RequestBody LoanRequestDto loanRequestDto)
			throws SavingAccountException, SavingAmountException {

		return new ResponseEntity<>(transactionService.loanPayment(loanRequestDto), HttpStatus.OK);
	}

}
