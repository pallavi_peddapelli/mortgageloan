package com.org.mortgage.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgage.dto.AccountDetailsResponseDto;
import com.org.mortgage.dto.LoginDetailsRequestDto;
import com.org.mortgage.dto.PropertyResponseDto;
import com.org.mortgage.dto.OccupationDetailsRequestDto;
import com.org.mortgage.dto.OccupationDetailsResponseDto;
import com.org.mortgage.dto.PersonalDetailsRequestDto;
import com.org.mortgage.dto.PersonalDetailsResponseDto;
import com.org.mortgage.dto.PropertyDto;
import com.org.mortgage.exception.DateOfBirthException;
import com.org.mortgage.exception.PropertyException;
import com.org.mortgage.exception.PropertyValueException;
import com.org.mortgage.exception.SalaryException;
import com.org.mortgage.exception.SquareFettDetailsException;
import com.org.mortgage.exception.UserException;
import com.org.mortgage.exception.UserNotEligibleException;
import com.org.mortgage.service.UserService;

@RequestMapping("/users")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping
	public ResponseEntity<PersonalDetailsResponseDto> userPersonalDetails(
			@Valid @RequestBody PersonalDetailsRequestDto personalDetailsRequestDto) throws DateOfBirthException {
		return new ResponseEntity<>(userService.userPersonalDetails(personalDetailsRequestDto), HttpStatus.OK);
	}

	@PostMapping("/occupations")
	public ResponseEntity<OccupationDetailsResponseDto> userOccupationDetails(
			@RequestBody OccupationDetailsRequestDto occupationDetailsRequestDto) throws UserException {
		return new ResponseEntity<>(userService.userOccupationDetails(occupationDetailsRequestDto), HttpStatus.OK);
	}

	@PostMapping("/properties")
	public ResponseEntity<PropertyResponseDto> propertyDetails(@RequestBody PropertyDto propertyDto)
			throws SquareFettDetailsException, UserNotEligibleException, DateOfBirthException, SalaryException,
			PropertyValueException, PropertyException {
		PropertyResponseDto loginResponseDto = userService.propertyDetailsIn(propertyDto);
		return new ResponseEntity<>(loginResponseDto, HttpStatus.ACCEPTED);
	}

	
	@PostMapping("/loginDetails")
	public ResponseEntity<AccountDetailsResponseDto> loginDetails(@RequestBody LoginDetailsRequestDto loginDetailsRequestDto)
			 {
		AccountDetailsResponseDto accountDetailsResponseDto = userService.loginDetails(loginDetailsRequestDto);
		return new ResponseEntity<>(accountDetailsResponseDto, HttpStatus.ACCEPTED);
	}

}
