package com.org.mortgage.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.mortgage.dto.AccountDetailsResponseDto;
import com.org.mortgage.dto.LoginDetailsRequestDto;
import com.org.mortgage.dto.PropertyResponseDto;
import com.org.mortgage.dto.OccupationDetailsRequestDto;
import com.org.mortgage.dto.OccupationDetailsResponseDto;
import com.org.mortgage.dto.PersonalDetailsRequestDto;
import com.org.mortgage.dto.PersonalDetailsResponseDto;
import com.org.mortgage.dto.PropertyDto;
import com.org.mortgage.entity.Account;
import com.org.mortgage.entity.Customer;
import com.org.mortgage.entity.Loan;
import com.org.mortgage.entity.OccupationDetails;
import com.org.mortgage.entity.Property;
import com.org.mortgage.entity.SquareFeetDetails;
import com.org.mortgage.entity.User;
import com.org.mortgage.exception.DateOfBirthException;
import com.org.mortgage.exception.PropertyException;
import com.org.mortgage.exception.PropertyValueException;
import com.org.mortgage.exception.SalaryException;
import com.org.mortgage.exception.SquareFettDetailsException;
import com.org.mortgage.exception.UserException;
import com.org.mortgage.exception.UserNotEligibleException;
import com.org.mortgage.repository.AccountRepository;
import com.org.mortgage.repository.CustomerRepository;
import com.org.mortgage.repository.LoanRepository;
import com.org.mortgage.repository.OccupationDetailsRepository;
import com.org.mortgage.repository.PropertyRepositry;
import com.org.mortgage.repository.SquareFeetDetailsRepositry;
import com.org.mortgage.repository.UserRepository;
import com.org.mortgage.utility.UserUtility;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	OccupationDetailsRepository occupationDetailsRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	SquareFeetDetailsRepositry squareFeetDetailsRepositry;

	@Autowired
	PropertyRepositry propertyRepositry;

	@Autowired
	LoanRepository loanRepository;
	
	@Autowired
	AccountRepository accountRepository;

	@Override
	public PersonalDetailsResponseDto userPersonalDetails(PersonalDetailsRequestDto personalDetailsRequestDto)
			throws DateOfBirthException {
		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();
		User user = new User();
		Date dateOfBirth = personalDetailsRequestDto.getDateOfBirth();
		Calendar c = Calendar.getInstance();
		c.setTime(dateOfBirth);
		int year = c.get(Calendar.YEAR);
		LocalDate today = LocalDate.now();
		int localYear = today.getYear();
		int years = localYear - year;
		if (years < 21) {
			throw new DateOfBirthException(UserUtility.DATE_OF_BIRTH_EXCEPTION);
		}
		BeanUtils.copyProperties(personalDetailsRequestDto, user);
		user = userRepository.save(user);
		personalDetailsResponseDto.setMessage(UserUtility.USER_PERSONAL_DETAILS);
		personalDetailsResponseDto.setStatusCode(UserUtility.USER_PERSONAL_DETAILS_STATUS);
		personalDetailsResponseDto.setUserId(user.getUserId());
		return personalDetailsResponseDto;
	}

	@Override
	public OccupationDetailsResponseDto userOccupationDetails(OccupationDetailsRequestDto occupationDetailsRequestDto)
			throws UserException {
		OccupationDetailsResponseDto occupationDetailsResponseDto = new OccupationDetailsResponseDto();
		OccupationDetails occupationDetails = new OccupationDetails();
		Optional<User> optionalUser = userRepository.findById(occupationDetailsRequestDto.getUserId());
		if (!optionalUser.isPresent()) {
			throw new UserException(UserUtility.USER_EXISTS_ERROR);
		}
		Optional<OccupationDetails> optionalOccupationalDetails = occupationDetailsRepository
				.findByUserId(occupationDetailsRequestDto.getUserId());
		if (optionalOccupationalDetails.isPresent()) {
			throw new UserException(UserUtility.USER_ALREADY_EXISTS_ERROR);
		}
		BeanUtils.copyProperties(occupationDetailsRequestDto, occupationDetails);
		occupationDetails = occupationDetailsRepository.save(occupationDetails);
		occupationDetailsResponseDto.setMessage(UserUtility.USER_OCCUPATIONAL_DETAILS);
		occupationDetailsResponseDto.setStatusCode(UserUtility.USER_OCCUPATIONAL_DETAILS_STATUS);
		occupationDetailsResponseDto.setUserId(occupationDetails.getUserId());
		return occupationDetailsResponseDto;
	}

	@Override
	public PropertyResponseDto propertyDetailsIn(PropertyDto propertyDto) throws SquareFettDetailsException,
			UserNotEligibleException, PropertyValueException, DateOfBirthException, SalaryException, PropertyException {
		
		int pincode = propertyDto.getPincode();
		String propertyName = propertyDto.getPropertyName();
		long squareFeetId = 0;
		SquareFeetDetails squareFeetDetails = squareFeetDetailsRepositry.findByPinCode(pincode);
		if (ObjectUtils.isEmpty(squareFeetDetails)) {
			throw new SquareFettDetailsException(UserUtility.SQUAREFEET_DETAILS_ERROR);
		}

		squareFeetId = squareFeetDetails.getSquareFeetDetailsId();
		long area = propertyDto.getArea();
		double perSquareFeetValue = squareFeetDetails.getPerSquareFeetValue();
		double propertyValue = perSquareFeetValue * area;

		if (propertyValue < propertyDto.getLoan()) {
			throw new PropertyValueException(UserUtility.PROPERTY_VALUE_EXCEPTION);
		}
		long userId = propertyDto.getUserId();
		
		Optional<OccupationDetails> occupationDetails = occupationDetailsRepository.findByUserId(userId);
		int months = propertyDto.getTenureMonths();
		double loan = propertyDto.getLoan();
		double salary = occupationDetails.get().getSalary();
		double rateOfInterest = 0.10 / 12;

		double emi = (loan * rateOfInterest * Math.pow(1 + rateOfInterest, months))
				/ (Math.pow(1 + rateOfInterest, months) - 1);
		if (emi >= salary * 0.8) {
			throw new SalaryException(UserUtility.SALARY_EXCEPTION);
		}
		if (propertyValue >= propertyDto.getLoan()  && emi <= salary * 0.8) {
			Optional<Property> Propertys = propertyRepositry.findByUserIdAndSquareFeetId(userId, squareFeetId);
			if (!ObjectUtils.isEmpty(Propertys)) {
				throw new PropertyException(UserUtility.PROPERTY_EXCEPTION);
			}
			Property property = new Property();
			property.setPropertyName(propertyName);
			property.setPropertyValue(propertyValue);
			property.setUserId(userId);
			property.setSquareFeetId(squareFeetId);
			propertyRepositry.save(property);
			PropertyResponseDto loginResponseDto = new PropertyResponseDto();
			double depositeValue = propertyValue*0.2;
			double mortgageAmount = -(propertyValue - depositeValue);
			loginResponseDto.setMortgageAmount(mortgageAmount);
			loginResponseDto.setEmiAmount(emi);
			loginResponseDto.setLoanAmount(propertyValue);
			loginResponseDto.setRateOfInterest(10);
			loginResponseDto.setTenureInMonths(months);
			loginResponseDto.setUserId(userId);
			loginResponseDto.setStatus(631);
			
			return loginResponseDto;
		} else {
			throw new UserNotEligibleException(UserUtility.USER_NOT_ELIGIBLE_ERROR);
		}

	}

	@Override
	public AccountDetailsResponseDto loginDetails(LoginDetailsRequestDto loginDetailsRequestDto) {
		
		AccountDetailsResponseDto accountDetailsResponseDto = new AccountDetailsResponseDto();
		
		long customerId = (long) (Math.random() * 1000L);
		long password = (long) (Math.random() * 10000L);
		String str = Long.toString(password);
		Customer customer = new Customer();
		customer.setPassword(str);
		customer.setUserId(loginDetailsRequestDto.getUserId());
		customer.setCustomerId(customerId);
		customerRepository.save(customer);
		
		Loan loanTb = new Loan();
		loanTb.setMortgageAmount(loginDetailsRequestDto.getMortgageAmount());
		loanTb.setEmiAmount(loginDetailsRequestDto.getEmiAmount());
		loanTb.setLoanAmount(loginDetailsRequestDto.getLoanAmount());
		loanTb.setRateOfInterest(loginDetailsRequestDto.getRateOfInterest());
		loanTb.setTenureInMonths(loginDetailsRequestDto.getTenureInMonths());
		loanTb.setUserId(loginDetailsRequestDto.getUserId());
		List<Property> propertys = propertyRepositry.findByUserId(loginDetailsRequestDto.getUserId());
		long propertyId = 0;
		for (Property property2 : propertys) {
			propertyId = property2.getPropertyId();
		}
		loanTb.setPropertyId(propertyId);
		System.out.println(propertyId);
		loanRepository.save(loanTb);
		Account account = new Account();
		long savingsAccount = (long) (Math.random() * 100000000L);
		long mortgageAccount = (long) (Math.random() * 100000000L);
		account.setSavingsAccount(savingsAccount);
		account.setMortgageAccount(mortgageAccount);
		account.setSavingsAmount(-loginDetailsRequestDto.getMortgageAmount());
		account.setMortgageAmount(loginDetailsRequestDto.getMortgageAmount());
		account.setUserId(loginDetailsRequestDto.getUserId());
		accountRepository.save(account);
		accountDetailsResponseDto.setCustomerId(customerId);
		accountDetailsResponseDto.setPassword(str);
		accountDetailsResponseDto.setStatus(630);
		return accountDetailsResponseDto;
	}
}