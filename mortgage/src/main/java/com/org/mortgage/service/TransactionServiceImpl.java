package com.org.mortgage.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgage.dto.LoanRequestDto;
import com.org.mortgage.dto.LoanResponseDto;
import com.org.mortgage.dto.TransactionDto;
import com.org.mortgage.dto.TransactionResponseDto;
import com.org.mortgage.entity.Account;
import com.org.mortgage.entity.Customer;
import com.org.mortgage.entity.Loan;
import com.org.mortgage.entity.Transaction;
import com.org.mortgage.exception.SavingAccountException;
import com.org.mortgage.exception.SavingAmountException;
import com.org.mortgage.repository.AccountRepository;
import com.org.mortgage.repository.CustomerRepository;
import com.org.mortgage.repository.LoanRepository;
import com.org.mortgage.repository.TransactionRepository;
import com.org.mortgage.utility.MortgageUtility;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	LoanRepository loanRepository;

	@Override
	public TransactionResponseDto transactionList(long customerId) {
		TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
		List<Transaction> transactionList = transactionRepository.findByCustomerId(customerId);
		if (transactionList == null) {
			List<TransactionDto> listOfTransactionDto = new ArrayList<>();
			transactionResponseDto.setListOfTransaction(listOfTransactionDto);
			transactionResponseDto.setStatus(613);
		}
		List<TransactionDto> listOfTransactionDto = new ArrayList<>();
		for (Transaction transaction : transactionList) {
			TransactionDto transactionDto = new TransactionDto();

			BeanUtils.copyProperties(transaction, transactionDto);
			if (listOfTransactionDto.size() <= 10) {
				listOfTransactionDto.add(transactionDto);

			}
		}

		transactionResponseDto.setListOfTransaction(listOfTransactionDto);
		transactionResponseDto.setStatus(614);
		return transactionResponseDto;

	}

	@Override
	public LoanResponseDto loanPayment(LoanRequestDto loanRequestDto)
			throws SavingAccountException, SavingAmountException {

		Optional<Customer> customer = customerRepository.findById(loanRequestDto.getCustomerId());

		Optional<Account> account = accountRepository.findBySavingsAccountAndUserId(loanRequestDto.getSavingsAccount(),
				customer.get().getUserId());

		if (!account.isPresent())
			throw new SavingAccountException(MortgageUtility.SAVING_ACCOUNT_NOT_FOUND);
		if (account.get().getSavingsAmount() <= loanRequestDto.getEmiAmonut()) {
			throw new SavingAmountException(MortgageUtility.SAVING_AMOUNT_NOT_SUFFICIENT);
		}

		Optional<Loan> loan = loanRepository.findByUserId(customer.get().getUserId());

		loan.get().setMortgageAmount(loan.get().getMortgageAmount() - loanRequestDto.getEmiAmonut());
		loanRepository.save(loan.get());

		double amount = account.get().getSavingsAmount() - loanRequestDto.getEmiAmonut();
		account.get().setSavingsAmount(amount);
		account.get().setMortgageAmount(loan.get().getMortgageAmount() + loanRequestDto.getEmiAmonut());
		accountRepository.save(account.get());

		Transaction transaction = new Transaction();
		transaction.setCustomerId(loanRequestDto.getCustomerId());
		transaction.setEmiAmonut(loanRequestDto.getEmiAmonut());
		transaction.setPaymentDate(new Date());
		transaction.setSavingsAmount(amount);
		
		transaction.setMortgageAmount(loan.get().getMortgageAmount() + loanRequestDto.getEmiAmonut());
		transactionRepository.save(transaction);

		LoanResponseDto loanResponseDto = new LoanResponseDto();

		loanResponseDto.setMessage("your loan payment done for this mounth");
		loanResponseDto.setStatusCode(631);

		return loanResponseDto;
	}
}
