package com.org.mortgage.service;

import com.org.mortgage.dto.AccountDetailsResponseDto;
import com.org.mortgage.dto.LoginDetailsRequestDto;
import com.org.mortgage.dto.PropertyResponseDto;
import com.org.mortgage.dto.OccupationDetailsRequestDto;
import com.org.mortgage.dto.OccupationDetailsResponseDto;
import com.org.mortgage.dto.PersonalDetailsRequestDto;
import com.org.mortgage.dto.PersonalDetailsResponseDto;
import com.org.mortgage.dto.PropertyDto;
import com.org.mortgage.exception.DateOfBirthException;
import com.org.mortgage.exception.PropertyException;
import com.org.mortgage.exception.SalaryException;
import com.org.mortgage.exception.SquareFettDetailsException;
import com.org.mortgage.exception.UserException;
import com.org.mortgage.exception.UserNotEligibleException;

public interface UserService {

	public PersonalDetailsResponseDto userPersonalDetails(PersonalDetailsRequestDto personalDetailsRequestDto)
			throws DateOfBirthException;

	public OccupationDetailsResponseDto userOccupationDetails(OccupationDetailsRequestDto occupationDetailsRequestDto)
			throws UserException;

	public PropertyResponseDto propertyDetailsIn(PropertyDto propertyDto)
			throws SquareFettDetailsException, UserNotEligibleException, DateOfBirthException, SalaryException,
			com.org.mortgage.exception.PropertyValueException, PropertyException;

	public AccountDetailsResponseDto loginDetails(LoginDetailsRequestDto loginDetailsRequestDto);

}
