package com.org.mortgage.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionResponseDto {
	private List<TransactionDto> listOfTransaction;
	private int status;
	public List<TransactionDto> getListOfTransaction() {
		return listOfTransaction;
	}
	public void setListOfTransaction(List<TransactionDto> listOfTransaction) {
		this.listOfTransaction = listOfTransaction;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
