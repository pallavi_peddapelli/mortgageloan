package com.org.mortgage.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class TransactionDto {
	private double emiAmonut;
	private Date paymentDate;
	private double savingsAmount;
	private double mortgageAmount;
	public double getEmiAmonut() {
		return emiAmonut;
	}
	public void setEmiAmonut(double emiAmonut) {
		this.emiAmonut = emiAmonut;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public double getSavingsAmount() {
		return savingsAmount;
	}
	public void setSavingsAmount(double savingsAmount) {
		this.savingsAmount = savingsAmount;
	}
	public double getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
}
