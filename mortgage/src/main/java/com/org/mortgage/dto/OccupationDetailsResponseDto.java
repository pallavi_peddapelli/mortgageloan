package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class OccupationDetailsResponseDto {
	private String message;
	private int statusCode;
	private long userId;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
}
