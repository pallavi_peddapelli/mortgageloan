package com.org.mortgage.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonalDetailsRequestDto {
	@NotEmpty(message = "User Name should not empty")
	private String userName;
	@NotEmpty(message = "Phone number should not empty")
	@Pattern(regexp = "(^$|[0-9]{10})", message = "phone number must be  10 digits")
	private String phoneNumber;
	//@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "Please provide a date of birth.")
	private Date dateOfBirth;
	@Size(min = 10, max = 10, message = "PAN card should be size of 10 length")
	private String panCard;
	@NotEmpty(message = "Gender should not empty")
	private String gender;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPanCard() {
		return panCard;
	}
	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

}
