package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoanResponseDto {
	private String message;
	private int statusCode;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
}
