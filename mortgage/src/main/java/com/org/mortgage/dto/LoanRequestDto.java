package com.org.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoanRequestDto {
	private long customerId;
	private long savingsAccount;
	private double emiAmonut;
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getSavingsAccount() {
		return savingsAccount;
	}
	public void setSavingsAccount(long savingsAccount) {
		this.savingsAccount = savingsAccount;
	}
	public double getEmiAmonut() {
		return emiAmonut;
	}
	public void setEmiAmonut(double emiAmonut) {
		this.emiAmonut = emiAmonut;
	}
	
}
