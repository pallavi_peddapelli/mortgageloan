package com.org.mortgage.dto;


public class CustomerResponseDto {
	private long savingsAccount;
	private double savingsAmount;
	private double mortgageAmount;
	private long userId;
	private int status;
	private long customerId;
	private double emiAmount;
	public long getSavingsAccount() {
		return savingsAccount;
	}
	public void setSavingsAccount(long savingsAccount) {
		this.savingsAccount = savingsAccount;
	}
	public double getSavingsAmount() {
		return savingsAmount;
	}
	public void setSavingsAmount(double savingsAmount) {
		this.savingsAmount = savingsAmount;
	}
	public double getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public double getEmiAmount() {
		return emiAmount;
	}
	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}
	
}
